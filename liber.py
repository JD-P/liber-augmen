import re
from sys import stderr
from bs4 import BeautifulSoup
from markdown import markdown
from argparse import ArgumentParser, FileType

parser = ArgumentParser()

parser.add_argument("infile", type=FileType('r'), help="The liber document to parse and html-ify.")
parser.add_argument("--warn", action='store_true', help="Flag potential errors such as unidirectional links.")
parser.add_argument("--suppress-output", action='store_true', help="Don't generate HTML.")

arguments = parser.parse_args()


def parse_header(header):
    header_entries = {}
    for entry in header.split("\n"):
        field, value = entry.split(":", maxsplit=1)
        header_entries[field.lower().replace(" ", "_")] = value.strip()
    return header_entries

document = BeautifulSoup("<html lang=\"en-US\"></html>", features='html.parser')
text = arguments.infile.read()
liber = text.split("\n\n\n")
header = parse_header(liber[0])
models_raw = liber[1:]

global link_database
link_database = dict()

class MiniModel:
    def __init__(self, model_text):
        """Parse a minimodel and instantiate a MiniModel object from it."""
        try:
            header_body, self.footer = model_text.split("---")
        except ValueError:
            header_body = model_text
            self.footer = ""
        header = header_body.split("\n\n")[0]
        self.title, self.id = header.strip().split("\n")
        link_database[self.id] = set(re.findall(" \[.+?\]\((.+?)\)", self.footer.split("\n")[-1]))
        self.body = '\n\n'.join(header_body.split("\n\n")[1:])
        
    def htmlify(self):
        """Return an HTML representation of the MiniModel."""
        soup = BeautifulSoup("<article></article>", features="html.parser")
        article = soup.article
        article["id"] = self.id

        title = document.new_tag("h2")
        title_link = document.new_tag("a", href=("#" + self.id))
        title_link.string = self.title
        title.append(title_link)
        article.append(title)
        body = document.new_tag("div")
        body['class'] = 'model-body'
        body.append(markdown(self.body))
        article.append(body)
        links = document.new_tag("div")
        links['class'] = 'links'
        footer_mod = re.sub("\[(.+)\] ", "<span class=\"link-type\">\[\g<1>\] </span>", self.footer)
        see_also = BeautifulSoup(markdown(footer_mod.split("\n")[-1]), 'html.parser')
        try:
            see_also.p['class'] = 'see-also'
        except TypeError:
            pass
        citations = document.new_tag("div")
        citations['class'] = 'citations'
        citations.append(markdown('\n'.join(footer_mod.split("\n")[:-1])))
        links.append(citations)
        links.append(see_also)
        article.append(links)
        return article
        
models = []
for model in models_raw:
    models.append(MiniModel(model))
    
html = document.html
html["lang"] = "en-US"

head = document.new_tag("head")
title = document.new_tag("title")
title.string = header["title"]
charset = document.new_tag("meta")
charset['charset'] = 'utf-8'
mobile_viewport = document.new_tag("meta")
mobile_viewport["name"] = "viewport"
mobile_viewport["content"] = "width=device-width"
css = document.new_tag("link")
css['rel'] = 'stylesheet'
css['type'] = 'text/css'
css['href'] = 'liber.css'
head.append(title)
head.append(charset)
head.append(mobile_viewport)
head.append(css)

body = document.new_tag("body")
body_header = document.new_tag("header")
body_title = document.new_tag("h1")
body_title.string = header["title"]
body_author = document.new_tag("p")
body_author.string = header["author"]
body_header.append(body_title)
if "subtitle" in header:
    body_subtitle = document.new_tag("h2")
    body_subtitle.string = header["subtitle"]
    body_header.append(body_subtitle)
body_header.append(body_author)
if "version" in header:
    body_version = document.new_tag("p")
    body_version.string = "Version " + header["version"]
    body_header.append(body_version)
if ("git_commit_link" in header) and ("last_updated" in header):
    updated_link = document.new_tag("a")
    updated_link["href"] = header["git_commit_link"]
    updated_link.string = "Updated " + header["last_updated"]
else:
    updated_link = "Updated " + header["last_updated"]
    
if "last_updated" in header:
    body_updated = document.new_tag("p")
    body_updated.append(updated_link)
    body_header.append(body_updated)

toc_nav = document.new_tag("nav")
toc_nav["id"] = "toc-container"

toc_label = document.new_tag("label")
toc_label["for"] = "toc-button"
toc_label["id"] = "toc-label"
toc_label.string = "&#9776;"

toc_mobile = document.new_tag("input")
toc_mobile["type"] = "checkbox"
toc_mobile["id"] = "toc-button"

toc = document.new_tag("ol")
toc["id"] = "toc"

for minimodel in models:
    entry = document.new_tag("li")
    entry_jump = document.new_tag("a")
    entry_jump.string = minimodel.title
    entry_jump["href"] = "#" + minimodel.id
    entry.append(entry_jump)
    toc.append(entry)

toc_nav.append(toc_mobile)
toc_nav.append(toc_label)
toc_nav.append(toc)

body.append(toc_nav)
body.append(body_header)

main = document.new_tag("main")

for minimodel in models:
    main.append(minimodel.htmlify())

body.append(main)
    
copyright = document.new_tag("footer")
copyright_notice = """
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
"""
copyright.string = copyright_notice
body.append(copyright)
    
html.append(head)
html.append(body)

if arguments.warn:
    for mid in link_database:
        error = False # Avoid printing blank lines for minimodels that don't have warnings
        for link in link_database[mid]:
            try:
                if ("#" + mid) not in link_database[link[1:]]:
                    error = True
                    print("[WARNING]: Link to {} in {} is not bidirectional.".format(link, ("#" + mid)), file=stderr)
            except KeyError:
                error = True
                print("[WARNING]: {} linked in {} but DOESN'T EXIST in document!".format(link, ("#" + mid)), file=stderr) 
        print("", file=stderr) if error else None
        
if not arguments.suppress_output:
    print(document.prettify(formatter=None))

